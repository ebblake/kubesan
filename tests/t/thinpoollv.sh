# SPDX-License-Identifier: Apache-2.0

# This test does not use ksan-supported-modes because it directly tests the
# ThinPoolLv CRD without using Volumes or StorageClass at all.

# delete_thin_lv <name> <index> <pool>
# Deletes a thin LV with a given name and its index in the ThinLvs[] array of
# the speicified pool.
delete_thin_lv() {
    name=$1
    index=$2
    pool=$3

    ksan-stage "Requesting deletion for thin LV \"$name\"..."
    kubectl patch --namespace kubesan-system thinpoollv $pool --type json --patch '
[
  {"op": "test", "path": "/spec/thinLvs/'$index'/name", "value": "'$name'"},
  {"op": "replace", "path": "/spec/thinLvs/'$index'/state/name", "value": "Removed"}
]
'
    ksan-poll 1 30 "[[ -z \"\$(kubectl get --namespace kubesan-system -o=jsonpath='{.status.thinLvs[?(@.name==\"$name\")].name}' thinpoollv $pool)\" ]]"

    ksan-stage "Removing thin LV \"$name\" from Spec..."
    kubectl patch --namespace kubesan-system thinpoollv $pool --type json --patch '
[
  {"op": "test", "path": "/spec/thinLvs/'$index'/name", "value": "'$name'"},
  {"op": "remove", "path": "/spec/thinLvs/'$index'"}
]
'
}

# check_active_on_node <pool> <expected>
# Check that the thinpoollv resource has the expected Status.ActiveOnNode.
check_active_on_node() {
    pool=$1
    expected=$2
    [[ $(kubectl get --namespace kubesan-system thinpoollv $pool --output jsonpath='{.status.activeOnNode}' 2>&1) == $expected ]]
}


ksan-stage "Creating empty ThinPoolLv..."

kubectl create -f - <<EOF
apiVersion: kubesan.gitlab.io/v1alpha1
kind: ThinPoolLv
metadata:
  name: thinpoollv
  namespace: kubesan-system
spec:
  vgName: kubesan-vg
  sizeBytes: 67108864
EOF

# Wait for Status.Conditions["Available"]
ksan-poll 1 30 '[[ "$(ksan-get-condition thinpoollv thinpoollv Available)" == True ]]'

ksan-stage "Creating thin LV..."
kubectl patch --namespace kubesan-system thinpoollv thinpoollv --type merge --patch "
spec:
  activeOnNode: $(__ksan-get-node-name 0)
  thinLvs:
    - name: thinlv
      contents:
        contentsType: Empty
      readOnly: false
      sizeBytes: 67108864
      state:
        name: Inactive
"
ksan-poll 1 30 "[[ -n \"\$(kubectl get --namespace kubesan-system -o=jsonpath='{.status.thinLvs[?(@.name==\"thinlv\")].name}' thinpoollv thinpoollv)\" ]]"

ksan-stage "Creating snapshot..."
kubectl patch --namespace kubesan-system thinpoollv thinpoollv --type merge --patch "
spec:
  thinLvs:
    - name: thinlv
      contents:
        contentsType: Empty
      readOnly: false
      sizeBytes: 67108864
      state:
        name: Inactive
    - name: thin-snapshot-123
      contents:
        contentsType: Snapshot
        snapshot:
          sourceThinLvName: thinlv
      readOnly: true
      sizeBytes: 0
      state:
        name: Inactive
"
ksan-poll 1 30 "[[ -n \"\$(kubectl get --namespace kubesan-system -o=jsonpath='{.status.thinLvs[?(@.name==\"thin-snapshot-123\")]}' thinpoollv thinpoollv)\" ]]"

check_active_on_node thinpoollv ""

ksan-stage "Activating thin LV..."
kubectl patch --namespace kubesan-system thinpoollv thinpoollv --type json --patch '
[
  {"op": "test", "path": "/spec/thinLvs/0/name", "value": "thinlv"},
  {"op": "replace", "path": "/spec/thinLvs/0/state/name", "value": "Active"}
]
'

ksan-poll 1 30 "check_active_on_node thinpoollv '$(__ksan-get-node-name 0)'"

ksan-stage "Switching the active node for the pool..."
kubectl patch --namespace kubesan-system thinpoollv thinpoollv --type json --patch '
[
  {"op": "replace", "path": "/spec/activeOnNode", "value": "'$(__ksan-get-node-name 1)'"}
]
'

ksan-poll 1 30 "check_active_on_node thinpoollv '$(__ksan-get-node-name 1)'"

ksan-stage "Creating base to serve as thin origin..."
kubectl patch --namespace kubesan-system thinpoollv thinpoollv --type merge --patch "
spec:
  thinLvs:
    - name: thinlv
      contents:
        contentsType: Empty
      readOnly: false
      sizeBytes: 67108864
      state:
        name: Active
    - name: thin-snapshot-123
      contents:
        contentsType: Snapshot
        snapshot:
          sourceThinLvName: thinlv
      readOnly: true
      sizeBytes: 0
      state:
        name: Inactive
    - name: thin-base-thinpool2
      contents:
        contentsType: Snapshot
        snapshot:
          sourceThinLvName: thin-snapshot-123
      readOnly: true
      sizeBytes: 0
      state:
        name: Inactive
"
ksan-poll 1 30 "[[ -n \"\$(kubectl get --namespace kubesan-system -o=jsonpath='{.status.thinLvs[?(@.name==\"thin-base-thinpool2\")]}' thinpoollv thinpoollv)\" ]]"

ksan-stage "Creating new pool with volume based on origin..."
kubectl create -f - <<EOF
apiVersion: kubesan.gitlab.io/v1alpha1
kind: ThinPoolLv
metadata:
  name: thinpool2
  namespace: kubesan-system
spec:
  vgName: kubesan-vg
  sizeBytes: 67108864
  activeOnNode: $(__ksan-get-node-name 0)
  sourcePool: thinpoollv
  thinLvs:
    - name: thinlv2
      contents:
        contentsType: Snapshot
        snapshot:
          sourceThinLvName: thinpoollv
      readOnly: false
      sizeBytes: $((68*1024*1024))
      state:
        name: Active
EOF

ksan-poll 1 30 "[[ \"\$(kubectl get --namespace kubesan-system -o=jsonpath='{.status.thinLvs[?(@.name==\"thinlv2\")].sizeBytes}' thinpoollv thinpool2)\" == $((68*1024*1024)) ]]"

check_active_on_node thinpoollv "$(__ksan-get-node-name 1)"
check_active_on_node thinpool2 "$(__ksan-get-node-name 0)"

# Cleanup; ksan-stage in delete_thin_lv
delete_thin_lv thinlv 0 thinpoollv
check_active_on_node thinpoollv ""

delete_thin_lv thin-snapshot-123 0 thinpoollv

delete_thin_lv thinlv2 0 thinpool2
check_active_on_node thinpool2 ""

delete_thin_lv thin-base-thinpool2 0 thinpoollv

ksan-stage "Deleting ThinPoolLv..."
kubectl delete --namespace kubesan-system thinpoollv thinpoollv thinpool2
ksan-poll 1 30 "[[ -z \"\$(kubectl get --namespace kubesan-system --no-headers --ignore-not-found thinpoollv thinpoollv thinpool2)\" ]]"
